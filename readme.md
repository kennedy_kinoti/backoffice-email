#iPay Africa Backoffice Email

## Getting Started
These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Prerequisites
- GO v1.10.4
- `chi` Routing Package
- `dep` Dependancy Manager
- `MySQL` Database

##Installing
- Copy files to your local machine
- Navigate to the file path in your computer
- You also need to have the following php packages installed
    
    ```
    go1.10.4
    ```

- Using the existing .env.example template, configure your environment variables and save the file .env

## Launching the program
- Run a go run *.go command in your terminal and open the corresponding address

## API End Points
#### Create User
    path : /create-user
    method : POST
    Fields: url, email_from, sender_name, API_KEY 
    
## Running the tests
Onging development and deployment tests

## Deployment
Not yet deployed to a live system

## Built With
- GoLang - The core backend language

## Authors
Kennedy Kinoti

## Acknowledgments
Hat tip to anyone whose code was used

As Socrates said:
> It’s not a bug – it’s an undocumented feature.

