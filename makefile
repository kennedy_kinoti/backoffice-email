run:
	go build . && ./email-service
build:
	go build -tags netgo -a -v
	sudo docker build -t "ipay/email-service" .
	sudo docker run -p 8080:8080 ipay/email-service