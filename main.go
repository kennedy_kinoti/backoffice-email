package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	_ "github.com/go-sql-driver/mysql"
	"github.com/joho/godotenv"
)

var router *chi.Mux
var db *sql.DB

// Merchant details struct
type Merchant struct {
	ID       int    `json:"id"`
	Email    string `json:"email"`
	UserName string `json:"username"`
	Password string `json:"password"`
	RcCode   string `json:"rc_code"`
}

// WHT struct
type WHT struct {
	Gross string `json:"gross"`
	Email  string `json:"email"`
	KRAPin string `json:"kra_pin"`
	Net    string `json:"net_amount"`
}

// Users struct
type Users struct {
	Name     string   `json:"name"`
	Email    string   `json:"email"`
	Username string   `json:"username"`
	Password string   `json:"password"`
	Roles    []string `json:"roles"`
}

func init() {
	router = chi.NewRouter()
	router.Use(middleware.Recoverer)

	var err error

	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading configuration from ENV")
	}

	dbName := os.Getenv("dbName")
	dbPass := os.Getenv("dbPass")
	dbHost := os.Getenv("dbHost")
	dbPort := os.Getenv("dbPort")

	dbSource := fmt.Sprintf("root:%s@tcp(%s:%s)/%s?charset=utf8", dbPass, dbHost, dbPort, dbName)
	db, err = sql.Open("mysql", dbSource)
	catch(err)
}

func routers() *chi.Mux {
	router.Get("/", ping)

	router.Post("/create-user", CreateMerchant)
	router.Post("/wht-email", WitholdingTax)

	router.Get("/health", Health)

	return router
}

// Health check function
func Health(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "OK %s", os.Getenv("emailFrom"))
}

// server starting point
func ping(w http.ResponseWriter, r *http.Request) {
	respondwithJSON(w, http.StatusOK, map[string]string{"message": "Welcome to iPay Email Service"})
}

//-------------- API ENDPOINT ------------------//

//CreateMerchant create a new merchant
func CreateMerchant(w http.ResponseWriter, r *http.Request) {
	// Declare the struct
	merchant := Merchant{}
	json.NewDecoder(r.Body).Decode(&merchant)

	go CreateMerchantEmail(merchant)

	respondwithJSON(w, http.StatusCreated, map[string]string{"message": "Merchant Data Sent Successfully to " + merchant.Email})

}

// WitholdingTax function
func WitholdingTax(w http.ResponseWriter, r *http.Request) {

	tax := WHT{}
	json.NewDecoder(r.Body).Decode(&tax)

	// return 54.2
	go WitholdingTaxEmail(tax)

	respondwithJSON(w, http.StatusCreated, map[string]string{"message": "Withdrawal Tax Email Sent"})
}

// CreateMerchantEmail HTML template
func CreateMerchantEmail(m Merchant) {
	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading configuration from ENV")
	}

	// nameData := m.BusinessName
	emailData := m.Email
	// vendorData := m.Vid
	userData := m.UserName
	passData := m.Password
	// hashData := m.HashKey

	url := os.Getenv("emailURL")
	from := os.Getenv("emailFrom")
	emailName := os.Getenv("emailFromName")
	emailAPI := os.Getenv("emailAPIKey")

	loc, _ := time.LoadLocation("Africa/Nairobi")
	t := time.Now().In(loc)
	time := t.Format("2006-01-02 15:04:05")

	postData := make(map[string]interface{})
	postData["from"] = from
	postData["fromName"] = emailName
	postData["apikey"] = emailAPI
	postData["subject"] = "eLipa Merchant, Karibu"
	postData["to"] = emailData
	postData["bodyHtml"] = `
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<title>Newsletter</title>
				<link rel="stylesheet" href="https://channels.intrepidprojects.co.ke/icons.css">
				<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
			</head>
			<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #eee;">
				<br>
				<div style="margin:0 auto !important;padding:20px; max-width: 500px;background-color: #fff;border-radius: 4px;">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td bgcolor="#ffffff" align="center">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:500px" class="m_-9170412488597135291wrapper">
										<tbody>
											<tr>
												<td align="center" valign="top" style="padding:15px 0 10px 0;font-size:11px;font-family:Helvetica,Arial,sans-serif" class="m_-9170412488597135291logo">
													<a target='_blank' href="https://ipayafrica.com" target="_blank">
														<div class="ipay_ipay" style="height:45px;background-position: center;"></div>
													</a>
												</td>
											</tr>
										</tbody>
									</table>

									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:500px" class="m_-9170412488597135291responsive-table">
										<tbody>
											<tr>
												<td bgcolor="#ffffff" align="center">

													<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:500px;border-bottom:1px dotted #aaa" class="m_-9170412488597135291wrapper">
														<tbody>
															<tr>
																<td align="center" valign="top" style="padding:10px 0;font-size:18px;color:#333;font-family:Helvetica,Arial,sans-serif;text-transform:uppercase" class="m_-9170412488597135291logo">
																	Merchant Backoffice
																</td>
															</tr>
														</tbody>
													</table>

													<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:500px" class="m_-9170412488597135291responsive-table">
														<tbody>
															<tr>
																<td>
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<br/>
																				<td align="center" style="font-family:Helvetica,Arial,sans-serif">
																					<p style="color:#999;text-align:center;margin:0">` + time + `</p>
																				</td>
																			</tr>
																			<tr>
																				<td align="left" style="padding:20px 0 30px 0;text-align:center;font-size:14px;line-height:25px;font-family:Helvetica,Arial,sans-serif;color:#666666" class="m_-9170412488597135291padding-copy">A user has successfully been created for your Backoffice.</td>
																			</tr>
																		</tbody>
																	</table>

																</td>
															</tr>
															<tr>
																<td style="padding:10px 0 0 0;border-top:1px dotted #aaa">

																	<table cellspacing="0" cellpadding="0" border="0" width="100%">

																		<tbody>
																			<tr>
																				<td valign="top" style="padding:0" class="m_-9170412488597135291mobile-wrapper">

																					<table cellspacing="0" cellpadding="0" style="width: 100%; padding: 5px;font-size: 14;font-family: Helvetica,Arial,sans-serif;color: #101010;">
																						<tr style="background-color: #eee;">
																							<td style="padding: 4px;">User Name:</td>
																							<td style="padding: 4px;">` + userData + `</td>
																						</tr>
																						<tr style="background-color: #eee;">
																							<td style="padding: 4px;">Email:</td>
																							<td style="padding: 4px;">` + emailData + `</td>
																						</tr>
																						<tr style="background-color: #eee;">
																							<td style="padding: 4px;">Password:</td>
																							<td style="padding: 4px;">` + passData + `</td>
																						</tr>
																					</table>
																					<br>

																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
															<tr>
																<td>

																</td>
															</tr>
															<tr>
																<td>

																</td>
															</tr>
															<tr>
																<td>

																</td>
															</tr>
															<tr>
																<td style="border-bottom:1px dotted #aaa">

																</td>
															</tr>
														</tbody>
													</table>

												</td>
											</tr>
										</tbody>
									</table>

								</td>
							</tr>

							<tr>
								<td bgcolor="#fff" align="center">

								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width:500px;margin: 0 auto;max-width: 500px;" class="m_-9170412488597135291responsive-table">
					<tbody>
						<tr>
							<td align="center" style="font-size:12px;line-height:18px;font-family:Helvetica,Arial,sans-serif;color:#666666; background-color: #eee;padding-top: 30px;padding-bottom: 30px;">
								Sent from <a target='_blank' href="https://ipayafrica.com" style="color:#00508f;text-decoration:none" target="_blank">ipayafrica.com</a>
								<br> Follow us on:
								<br>

								<a href="https://www.facebook.com/IpayAfric/" target="_blank"><i class="fab fa-facebook" style="font-size:40px;color:#124aa1;"></i></a>
								<a href="https://twitter.com/ipayafrica" target="_blank"><i class="fab fa-twitter-square" style="font-size:40px;color:#124aa1;"></i></a>
								<a href="https://www.linkedin.com/company/ipay-limited" target="_blank"><i class="fab fa-linkedin" style="font-size:40px;color:#124aa1;"></i></a>

							</td>
						</tr>
					</tbody>
				</table>
			</body>

			</html>
		`
	messages := make(chan string)

	go postCurl(messages, url, postData)
}

// WitholdingTaxEmail HTML template
func WitholdingTaxEmail(tax WHT) {
	if err := godotenv.Load(); err != nil {
		log.Println("File .env not found, reading configuration from ENV")
	}

	// nameData := m.BusinessName
	emailData := tax.Email
	// with
	gross := tax.Gross
	net := tax.Net
	kraPin := tax.KRAPin
	// taxRate := os.Getenv("withholdingtaxrate")
	taxRate := "0.05"

	url := os.Getenv("emailURL")
	from := os.Getenv("emailFrom")
	emailName := os.Getenv("emailFromName")
	emailAPI := os.Getenv("emailAPIKey")

	loc, _ := time.LoadLocation("Africa/Nairobi")
	t := time.Now().In(loc)
	time := t.Format("2006-01-02 15:04:05")

	postData := make(map[string]interface{})
	postData["from"] = from
	postData["fromName"] = emailName
	postData["apikey"] = emailAPI
	postData["subject"] = "iPay Affiliate, Withdraw"
	postData["to"] = emailData
	postData["bodyHtml"] = `
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				<title>Newsletter</title>
				<link rel="stylesheet" href="https://channels.intrepidprojects.co.ke/icons.css">
				<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
			</head>
			<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="background-color: #eee;">
				<br>
				<div style="margin:0 auto !important;padding:20px; max-width: 500px;background-color: #fff;border-radius: 4px;">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tbody>
							<tr>
								<td bgcolor="#ffffff" align="center">
									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:500px" class="m_-9170412488597135291wrapper">
										<tbody>
											<tr>
												<td align="center" valign="top" style="padding:15px 0 10px 0;font-size:11px;font-family:Helvetica,Arial,sans-serif" class="m_-9170412488597135291logo">
													<a target='_blank' href="https://ipayafrica.com" target="_blank">
														<div class="ipay_ipay" style="height:45px;background-position: center;"></div>
													</a>
												</td>
											</tr>
										</tbody>
									</table>

									<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:500px" class="m_-9170412488597135291responsive-table">
										<tbody>
											<tr>
												<td bgcolor="#ffffff" align="center">

													<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:500px;border-bottom:1px dotted #aaa" class="m_-9170412488597135291wrapper">
														<tbody>
															<tr>
																<td align="center" valign="top" style="padding:10px 0;font-size:18px;color:#333;font-family:Helvetica,Arial,sans-serif;text-transform:uppercase" class="m_-9170412488597135291logo">
																	iPay Africa Affiliate
																</td>
															</tr>
														</tbody>
													</table>

													<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:500px" class="m_-9170412488597135291responsive-table">
														<tbody>
															<tr>
																<td>
																	<table width="100%" border="0" cellspacing="0" cellpadding="0">
																		<tbody>
																			<tr>
																				<br/>
																				<td align="center" style="font-family:Helvetica,Arial,sans-serif">
																					<p style="color:#999;text-align:center;margin:0">` + time + `</p>
																				</td>
																			</tr>
																			<tr>
																				<td align="left" style="padding:20px 0 30px 0;text-align:center;font-size:14px;line-height:25px;font-family:Helvetica,Arial,sans-serif;color:#666666" class="m_-9170412488597135291padding-copy">A withdrawal has successfully been made from iPay Affiliate .</td>
																			</tr>
																		</tbody>
																	</table>

																</td>
															</tr>
															<tr>
																<td style="padding:10px 0 0 0;border-top:1px dotted #aaa">

																	<table cellspacing="0" cellpadding="0" border="0" width="100%">

																		<tbody>
																			<tr>
																				<td valign="top" style="padding:0" class="m_-9170412488597135291mobile-wrapper">

																					<table cellspacing="0" cellpadding="0" style="width: 100%; padding: 5px;font-size: 14;font-family: Helvetica,Arial,sans-serif;color: #101010;">
																						<tr style="background-color: #eee;">
																							<td style="padding: 4px;">User Email:</td>
																							<td style="padding: 4px;">` + emailData + `</td>
																						</tr>
																						<tr style="background-color: #eee;">
																							<td style="padding: 4px;">KRA Pin:</td>
																							<td style="padding: 4px;">` + kraPin + `</td>
																						</tr>
																						<tr style="background-color: #eee;">
																							<td style="padding: 4px;">Gross Amount:</td>
																							<td style="padding: 4px;">` + gross + `</td>
																						</tr>
																						<tr style="background-color: #eee;">
																							<td style="padding: 4px;">Witholding Tax:</td>
																							<td style="padding: 4px;">` + taxRate + `</td>
																						</tr>
																						<tr style="background-color: #eee;">
																							<td style="padding: 4px;">Net Amount Less Sender Charge:</td>
																							<td style="padding: 4px;">` + net + `</td>
																						</tr>
																					</table>
																					<br>

																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
															<tr>
																<td>

																</td>
															</tr>
															<tr>
																<td>

																</td>
															</tr>
															<tr>
																<td>

																</td>
															</tr>
															<tr>
																<td style="border-bottom:1px dotted #aaa">

																</td>
															</tr>
														</tbody>
													</table>

												</td>
											</tr>
										</tbody>
									</table>

								</td>
							</tr>

							<tr>
								<td bgcolor="#fff" align="center">

								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<table width="100%" border="0" cellspacing="0" cellpadding="0" align="center" style="max-width:500px;margin: 0 auto;max-width: 500px;" class="m_-9170412488597135291responsive-table">
					<tbody>
						<tr>
							<td align="center" style="font-size:12px;line-height:18px;font-family:Helvetica,Arial,sans-serif;color:#666666; background-color: #eee;padding-top: 30px;padding-bottom: 30px;">
								Sent from <a target='_blank' href="https://ipayafrica.com" style="color:#00508f;text-decoration:none" target="_blank">ipayafrica.com</a>
								<br> Follow us on:
								<br>

								<a href="https://www.facebook.com/IpayAfric/" target="_blank"><i class="fab fa-facebook" style="font-size:40px;color:#124aa1;"></i></a>
								<a href="https://twitter.com/ipayafrica" target="_blank"><i class="fab fa-twitter-square" style="font-size:40px;color:#124aa1;"></i></a>
								<a href="https://www.linkedin.com/company/ipay-limited" target="_blank"><i class="fab fa-linkedin" style="font-size:40px;color:#124aa1;"></i></a>

							</td>
						</tr>
					</tbody>
				</table>
			</body>

			</html>
		`
	messages := make(chan string)

	go postCurl(messages, url, postData)
}

// postCurl
func postCurl(messages chan<- string, url string, postFields map[string]interface{}) {

	postFieldsString := ""

	for k, v := range postFields {
		if postFieldsString == "" {
			postFieldsString = k + "=" + fmt.Sprintf("%v", v)
		} else {
			postFieldsString = postFieldsString + "&" + k + "=" + fmt.Sprintf("%v", v)
		}
	}

	// log.Println(postFieldsString)

	payload := strings.NewReader(postFieldsString)
	req, _ := http.NewRequest("POST", url, payload)
	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	req.Header.Add("cache-control", "no-cache")
	res, _ := http.DefaultClient.Do(req)

	if res != nil {
		defer res.Body.Close()
	}

	body, _ := ioutil.ReadAll(res.Body)

	response := string(body)
	// fmt.Println(response)
	messages <- response
}

func main() {
	routers()

	// http.ListenAndServe(":8091", Logger())
	if err := godotenv.Load(); err != nil {
		fmt.Println("File .env not found, reading port configuration from ENV")
	}

	port := os.Getenv("port")

	http.ListenAndServe(":"+port, Logger())

}
